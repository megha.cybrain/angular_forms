import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  signupForm!:FormGroup;

  constructor(private fb: FormBuilder, private router :Router) { 
  }

  ngOnInit() {
    // Create the reactive form
    this.signupForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      address: ['', Validators.required]
    });
  }
  onSubmit():void{
    if(this.signupForm.valid){
      console.log(this.signupForm.value);
      this.router.navigate(['/dashboard/analytics'])
    }
    else{
      this.signupForm.markAllAsTouched()
    }
  }

}
